import 'package:injectable/injectable.dart';
import 'package:example_clean_complete/domain/entities/auth_entity.dart';
import 'package:example_clean_complete/domain/entities/user_entity.dart';
import 'package:example_clean_complete/domain/repositories/auth/auth_repository.dart';
import 'package:example_clean_complete/domain/usecases/base/base_future_use_case.dart';

@injectable
class LoginUserEmailUseCase extends BaseFutureUseCase<AuthEntity, UserEntity> {
  final AuthRepository _repository;

  LoginUserEmailUseCase(this._repository);

  @override
  Future<UserEntity> call([AuthEntity params]) =>
      _repository.loginUserEmail(params.email, params.password);
}
