import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:example_clean_complete/app/pages/home/home_module.dart';
import 'package:example_clean_complete/app/pages/login/login_module.dart';
import 'package:example_clean_complete/app/pages/splash/splash_module.dart';
import 'package:example_clean_complete/app/stores/auth/auth_store.dart';

import 'app_widget.dart';

class AppModule extends MainModule {
  // here will be any class you want to inject into your project (eg bloc, dependency)
  @override
  List<Bind> get binds => [
        Bind((i) => AuthStore()),
      ];

  // here will be the routes of your module
  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          module: SplashModule(),
          transition: TransitionType.noTransition,
        ),
        ModularRouter('/login', module: LoginModule()),
        ModularRouter('/home', module: HomeModule()),
      ];

  // add your main widget here
  @override
  Widget get bootstrap => AppWidget();
}
