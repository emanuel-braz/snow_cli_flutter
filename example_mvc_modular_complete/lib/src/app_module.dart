import 'package:example_mvc_modular_complete/src/modules/login/login_module.dart';
import 'package:example_mvc_modular_complete/src/shared/auth/auth_store.dart';
import 'package:example_mvc_modular_complete/src/shared/auth/repositories/auth_repository.dart';
import 'package:example_mvc_modular_complete/src/shared/auth/repositories/auth_repository_interface.dart';
import 'package:example_mvc_modular_complete/src/shared/clients/dio_client.dart';
import 'package:example_mvc_modular_complete/src/shared/clients/hive_client.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'app_widget.dart';
import 'modules/home/home_module.dart';
import 'modules/splash/splash_page.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => DioClient()),
        Bind<AuthRepository>((i) => AuthRepositoryDefault()),
        Bind((i) => HiveClient()),
        Bind((i) => AuthStore(), singleton: true),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          Modular.initialRoute,
          child: (context, args) => SplashPage(),
          transition: TransitionType.noTransition,
        ),
        ModularRouter('/login', module: LoginModule()),
        ModularRouter('/home', module: HomeModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
