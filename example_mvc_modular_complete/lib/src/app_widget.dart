import 'package:flutter/material.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:example_mvc_modular_complete/src/styles/app_theme_data.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SnowMaterialApp(
      theme: AppThemeData.themeDataLight,
      darkTheme: AppThemeData.themeDataDark,
    );
  }
}
