import 'package:example_mvc_modular_complete/src/shared/auth/auth_store.dart';
import 'package:example_mvc_modular_complete/src/shared/models/user_model.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'login_controller.g.dart';

class LoginController = _LoginBase with _$LoginController;

abstract class _LoginBase with Store {
  final authStore = Modular.get<AuthStore>();

  @observable
  bool loading = false;

  @action
  Future<bool> loginWithEmail(String email, String password) async {
    UserModel user;
    try {
      loading = true;
      user = await authStore.loginWithEmail(email, password);
    } catch (e) {} finally {
      loading = false;
    }
    return user != null;
  }
}
