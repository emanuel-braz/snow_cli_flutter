// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:example_clean/data/remote/dio_client.dart';
import 'package:example_clean/data/data_sources/feature/feature_remote_data_source.dart';
import 'package:example_clean/data/local/hive_client.dart';
import 'package:example_clean/data/data_sources/feature/feature_local_data_source.dart';
import 'package:example_clean/data/repositories/feature/feature_repository_impl.dart';
import 'package:example_clean/domain/repositories/feature/feature_repository.dart';
import 'package:example_clean/domain/usecases/feature/do_something_use_case.dart';
import 'package:get_it/get_it.dart';

void $initGetIt(GetIt g, {String environment}) {
  g.registerFactory<FeatureRemoteDataSource>(
      () => FeatureRemoteDataSource(g<DioClient>()));
  g.registerFactory<FeatureLocalDataSource>(
      () => FeatureLocalDataSource(g<HiveClient>()));
  g.registerFactory<DoSomethingUseCase>(
      () => DoSomethingUseCase(g<FeatureRepository>()));

  //Eager singletons must be registered in the right order
  g.registerSingleton<DioClient>(DioClient());
  g.registerSingleton<HiveClient>(HiveClient());
  g.registerSingleton<FeatureRepository>(FeatureRepositoryImpl(
      g<FeatureRemoteDataSource>(), g<FeatureLocalDataSource>()));
}
