import 'package:flutter_modular/flutter_modular.dart';
import 'package:injectable/injectable.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:example_clean/data/constants/constants.dart';
import 'package:example_clean/data/helpers/error_mapper.dart';

import 'app/app_module.dart';
import 'di/di.dart';

void main() {
  RunAppSnow(
    ModularApp(
      module: AppModule(),
    ),
    flavorValues: Constants.flavorDev,
    flavor: Flavor.dev,
    getItInit: () {
      Resource.setErrorMapper(ErrorMapper.from);
      configureInjection(dev.name);
    },
    enableDevicePreview: false,
  );
}
