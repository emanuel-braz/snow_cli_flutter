import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:example_clean/data/constants/constants.dart';
import 'package:example_clean/data/helpers/error_mapper.dart';

import 'app/app_module.dart';
import 'app_flavor_values.dart';
import 'di/di.dart';

void main() {
  RunAppSnow(
    ModularApp(
      module: AppModule(),
    ),
    flavorValues: Constants.flavorQa,
    getItInit: () {
      Resource.setErrorMapper(ErrorMapper.from);
      configureInjection(qa.name);
    },
    flavor: Flavor.qa,
  );
}
