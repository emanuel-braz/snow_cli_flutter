import 'package:flutter/foundation.dart';

@immutable
class TodoEntity {
  const TodoEntity();

  @override
  String toString() => 'TodoInfo()'; // TODO Write toString of Todo

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TodoEntity; // TODO Write == operator of Todo
  }

  //@override
  //int get hashCode => property.hashCode; // TODO hashCode overrides of Todo

}
