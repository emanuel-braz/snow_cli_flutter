import 'dart:convert';

import 'package:example_clean/domain/entities/todo/todo_entity.dart';

extension TodoMapper on TodoEntity {
  TodoEntity copyWith(
      // TODO Write copyWith of Todo
      ) {
    return TodoEntity(
        // TODO Write copyWith of Todo
        );
  }

  Map<String, dynamic> toMap() {
    return {
      // TODO Write toMap of Todo
    };
  }

  TodoEntity fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TodoEntity(
        // TODO Write fromMap of Todo
        );
  }

  String toJson() => json.encode(toMap());

  TodoEntity fromJson(String source) => fromMap(json.decode(source));
}
