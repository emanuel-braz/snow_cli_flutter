import 'package:injectable/injectable.dart';
import 'package:example_clean/data/local/hive_client.dart';

@injectable
class TodoLocalDataSource {
  final HiveClient _hiveClient;
  static const String box = "Todo";

  const TodoLocalDataSource(this._hiveClient);
}
