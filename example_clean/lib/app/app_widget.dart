import 'package:flutter/material.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:example_clean/app/styles/app_theme_data.dart';

class AppWidget extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return SnowMaterialApp(
      title: 'Flutter Demo',
      theme: AppThemeData.themeDataLight,
      darkTheme: AppThemeData.themeDataDark,
      initialRoute: '/',
    );
  }
}
