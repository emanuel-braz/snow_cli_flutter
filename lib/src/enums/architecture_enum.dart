enum Architecture { cleanArchitecture, mvcModular }

const defaultArchitecture = Architecture.cleanArchitecture;

extension ArchitectureMapper on Architecture {
  static Architecture fromString(String architecture) {
    switch (architecture) {
      case 'clean':
        return Architecture.cleanArchitecture;
        break;
      case 'mvc_modular':
        return Architecture.mvcModular;
        break;
      default:
        return null;
    }
  }
}
