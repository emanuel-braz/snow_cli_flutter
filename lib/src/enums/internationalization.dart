enum Internationalization { i18n, flutterIntl }

extension InternationalizationMapper on Internationalization {
  static Internationalization fromString(String architecture) {
    switch (architecture) {
      case 'i18n':
        return Internationalization.i18n;
        break;
      case 'flutterIntl':
        return Internationalization.flutterIntl;
        break;
      default:
        return null;
    }
  }
}
