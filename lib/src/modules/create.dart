import 'dart:convert';
import 'dart:io';

import 'package:flutter_snow_blower/flutter_snow_blower.dart';
import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';
import 'package:flutter_snow_blower/src/enums/internationalization.dart';
import 'package:flutter_snow_blower/src/enums/persistence_enum.dart';
import 'package:flutter_snow_blower/src/utils/utils.dart' show mainDirectory;

Future<void> create({
  String projectName,
  String projectDescription,
  String projectOrg,
  bool isKotlin,
  bool isSwift,
  bool isAndroidX,
  Architecture architecture,
  Persistence persistence,
  Internationalization internationalization,
  bool complete,
  bool useEnvironment,
}) {
  return startFlutterCreate(
    projectName: projectName,
    projectDescription: projectDescription,
    projectOrg: projectOrg,
    isKotlin: isKotlin,
    isSwift: isSwift,
    isAndroidX: isAndroidX,
    architecture: architecture,
    persistence: persistence,
    complete: complete,
    internationalization: internationalization,
    useEnvironment: useEnvironment,
  );
}

Future<void> startFlutterCreate({
  String projectName,
  String projectDescription,
  String projectOrg,
  bool isKotlin,
  bool isSwift,
  bool isAndroidX,
  Architecture architecture,
  Persistence persistence,
  Internationalization internationalization,
  bool complete,
  bool useEnvironment,
}) {
  mainDirectory = projectName + '/';

  var flutterArgs = createFlutterArgs(
    projectName,
    projectDescription,
    projectOrg,
    isKotlin,
    isSwift,
    isAndroidX,
  );

  return startSnowCreate(
    projectName,
    architecture,
    persistence,
    internationalization,
    complete,
    useEnvironment,
    () async {
      // Verify if the flutter is in stable channel to mantains the androidx
      // arguments, otherwize, remove it.
      final versionFlutterProcess =
          await Process.start("flutter", ["--version"], runInShell: true);
      final versionFlutter =
          await utf8.decodeStream(versionFlutterProcess.stdout);
      if (!versionFlutter.contains("stable")) {
        flutterArgs.remove("--androidx");
      }
      return Process.start('flutter', flutterArgs, runInShell: true);
    },
  );
}

Future<void> startSnowCreate(
  String projectName,
  Architecture architecture,
  Persistence persistence,
  Internationalization internationalization,
  bool complete,
  bool useEnvironment,
  Future<Process> Function() createProject,
) {
  return start(
    completeStart: complete,
    force: true,
    dir: Directory('$projectName/lib'),
    architecture: architecture,
    persistence: persistence,
    internationalization: internationalization,
    useEnvironment: useEnvironment,
    createProject: createProject,
  );
}

List<String> createFlutterArgs(
  String projectName,
  String projectDescription,
  String projectOrg,
  bool isKotlin,
  bool isSwift,
  bool isAndroidX,
) {
  projectDescription = projectDescription ??
      "A new Flutter project. Created by Snowman Labs with Flutter Snow Blower";
  projectOrg = projectOrg ?? "com.snowmanlabs";

  var flutterArgs = ['create'];
  flutterArgs.add('--no-pub');

  if (isKotlin) {
    flutterArgs.add('-a');
    flutterArgs.add('kotlin');
  }

  if (isSwift) {
    flutterArgs.add('-i');
    flutterArgs.add('swift');
  }

  if (isAndroidX) {
    flutterArgs.add('--androidx');
  }

  flutterArgs.add('--project-name');
  flutterArgs.add("$projectName");

  if (projectDescription.isNotEmpty) {
    flutterArgs.add('--description');
    flutterArgs.add("'$projectDescription'");
  }

  if (projectOrg.isNotEmpty) {
    flutterArgs.add('--org');
    flutterArgs.add("$projectOrg");
  }

  flutterArgs.add(projectName);
  return flutterArgs;
}
