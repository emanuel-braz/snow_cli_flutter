import 'package:flutter_snow_blower/src/enums/internationalization.dart';
import 'package:flutter_snow_blower/src/modules/start/select_option.dart';

Future<Internationalization> chooseInternationalization() async {
  var selected = selectOption(
    'What internationalization package do you want to use?',
    ['i18n (default)', 'Flutter Intl'],
  );

  switch (selected) {
    case 0:
      return Internationalization.i18n;
    case 1:
      return Internationalization.flutterIntl;
    default:
      return Internationalization.i18n;
  }
}
