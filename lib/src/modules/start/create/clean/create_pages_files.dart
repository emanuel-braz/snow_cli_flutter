part of './create_clean_architecture.dart';

Future<void> _createPagesFiles(String package, bool complete,
    {Internationalization internationalization = Internationalization.i18n}) async {
  await Generate.module('app/pages/home', true, false);

  if (internationalization == Internationalization.i18n) {
    createStaticFile(
      libPath('app/i18n/validators_i18n.dart'),
      templates.validatorsI18n(),
    );
  }

  // Utils
  if (internationalization == Internationalization.i18n) {
    createStaticFile(
      libPath('app/utils/validators.dart'),
      templates.validators(Architecture.cleanArchitecture),
    );
  }

  if (complete) {
    // ** splash
    if (internationalization == Internationalization.flutterIntl) {
      createStaticFile(
        libPath('modules/splash/splash_page.dart'),
        templates.splashPageModularWithFlutterIntl(package),
      );
    } else {
      createStaticFile(
        libPath('modules/splash/splash_page.dart'),
        templates.splashPageModular(package),
      );
    }

    createStaticFile(
      libPath('app/pages/splash/splash_controller.dart'),
      templates.splashControllerClean(package),
    );

    createStaticFile(libPath('app/pages/splash/splash_module.dart'),
        templates.splashModule());

    // ** login
    createStaticFile(
      libPath('app/pages/login/login_module.dart'),
      templates.loginModule(),
    );

    // i18n
    if (internationalization == Internationalization.i18n) {
      createStaticFile(
        libPath('app/i18n/login_i18n.dart'),
        templates.loginI18n(),
      );
    }

    // widget
    createStaticFile(
      libPath('app/widgets/login/email_text_field_widget.dart'),
      templates.emailTextFieldWidget(
          package, "app", Architecture.cleanArchitecture),
    );
    createStaticFile(
      libPath('app/widgets/login/password_text_field_widget.dart'),
      templates.passwordTextFieldWidget(
          package, "app", Architecture.cleanArchitecture),
    );

    // page
    createStaticFile(
      libPath('app/pages/login/pages/login/login_page.dart'),
      internationalization == Internationalization.i18n
          ? templates.loginPage(package, Architecture.cleanArchitecture)
          : templates.loginPageWithFlutterIntl(
              package, Architecture.cleanArchitecture),
    );

    createStaticFile(
      libPath('app/pages/login/pages/login/login_controller.dart'),
      templates.loginControllerClean(package),
    );
    // ** register
    createStaticFile(
      libPath('app/pages/login/pages/register/register_page.dart'),
      internationalization == Internationalization.i18n
          ? templates.registerPage(package, Architecture.cleanArchitecture)
          : templates.registerPageWithFlutterIntl(
              package, Architecture.cleanArchitecture),
    );
    createStaticFile(
      libPath('app/pages/login/pages/register/register_controller.dart'),
      templates.registerControllerClean(package),
    );
  }
}
