part of './create_clean_architecture.dart';

void _createDomainFiles(String package, bool complete) {
  createStaticFile(
    libPath('domain/usecases/base/base_future_use_case.dart'),
    templates.baseFutureUseCase(),
  );
  createStaticFile(
    libPath('domain/usecases/base/base_stream_use_case.dart'),
    templates.baseStreamUseCase(),
  );
  createStaticFile(
    libPath('domain/usecases/base/base_use_case.dart'),
    templates.baseUseCase(),
  );

  createStaticFile(
    libPath('domain/entities/user_entity.dart'),
    templates.userEntity(),
  );

  if (complete) {
    createStaticFile(
      libPath('domain/entities/auth_entity.dart'),
      templates.authEntity(),
    );
    createStaticFile(
      libPath('domain/entities/auth_status.dart'),
      templates.authStatus(),
    );

    createStaticFile(
      libPath('domain/repositories/auth/auth_repository.dart'),
      templates.authRepositoryClean(package),
    );

    createStaticFile(
      libPath('domain/usecases/auth/get_auth_status_stream_use_case.dart'),
      templates.getAuthStatusStreamUseCase(package),
    );
    createStaticFile(
      libPath('domain/usecases/auth/get_user_stream_use_case.dart'),
      templates.getUserStreamUseCase(package),
    );
    createStaticFile(
      libPath('domain/usecases/auth/get_user_use_case.dart'),
      templates.getUserUseCase(package),
    );
    createStaticFile(
      libPath('domain/usecases/auth/login_user_email_use_case.dart'),
      templates.loginUserEmailUseCase(package),
    );
    createStaticFile(
      libPath('domain/usecases/auth/logout_user_use_case.dart'),
      templates.logoutUserUseCase(package),
    );
    createStaticFile(
      libPath('domain/usecases/auth/register_user_email_use_case.dart'),
      templates.registerUserEmailUseCase(package),
    );
  } else {
    //FeatureRepository
    createStaticFile(
      libPath('domain/repositories/feature/feature_repository.dart'),
      templates.featureRepositoryClean(package),
    );
    //FeatureUseCase
    createStaticFile(
      libPath('domain/usecases/feature/do_something_use_case.dart'),
      templates.doSomethingUseCase(package),
    );
    //UserEntity
    createStaticFile(
      libPath('domain/usecases/entities/user_entity.dart.dart'),
      templates.userEntity(),
    );
  }
}
