part of './create_clean_architecture.dart';

void _createTestFiles(String package, bool complete, String dirPath) {
  dirPath = dirPath.replaceFirst('lib', '');
  createStaticFile(
    '${dirPath}test/data/remote/dio_client_test.dart',
    templates.dioClientTest(package, Architecture.cleanArchitecture),
  );
  createStaticFile(
    '${dirPath}test/test_variants/flavors_test_variant.dart',
    templates.flavorsTestVariant(package, Architecture.cleanArchitecture),
  );
  createStaticFile(
    '${dirPath}test/drawer_features_example_test.dart',
    templates.drawerFeaturesExampleTest(
        package, Architecture.cleanArchitecture),
  );
  if (complete) {
    createStaticFile(
      '${dirPath}test/app/i18n/i18n_test.dart',
      templates.i18nTest(package, Architecture.cleanArchitecture),
    );
  }
}
