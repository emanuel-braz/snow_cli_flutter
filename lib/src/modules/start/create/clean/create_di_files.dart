part of './create_clean_architecture.dart';

void _createDiFiles(String package, bool complete) {
  createStaticFile(
    libPath('di/di.dart'),
    templates.di(),
  );
  createStaticFile(
    libPath('di/modules/local_module.dart'),
    templates.localModuleClean(complete),
  );
  createStaticFile(
    libPath('di/modules/remote_module.dart'),
    templates.remoteModuleClean(complete),
  );
}
