part of './../create.dart';

void _createMainFile(
  String dirPath,
  String package, {
  String pathToAppModule = 'app',
  bool useEnviroment = true,
  Architecture architecture,
}) {
  switch (architecture) {
    case Architecture.cleanArchitecture:
      if (!useEnviroment) {
        createStaticFile(
          '$dirPath/main.dart',
          templates.startMainModular(package, pathToAppModule),
        );
      } else {
        createStaticFile(
          '$dirPath/main_dev.dart',
          templates.startMainDevClean(package, pathToAppModule),
        );
        createStaticFile(
          '$dirPath/main_production.dart',
          templates.startMainProductionClean(package, pathToAppModule),
        );
        createStaticFile(
          '$dirPath/main_qa.dart',
          templates.startMainQaClean(package, pathToAppModule),
        );
      }
      break;
    case Architecture.mvcModular:
      if (!useEnviroment) {
        createStaticFile(
          '$dirPath/main.dart',
          templates.startMainModular(package, pathToAppModule),
        );
      } else {
        createStaticFile(
          '$dirPath/main_dev.dart',
          templates.startMainDevModular(package, pathToAppModule),
        );
        createStaticFile(
          '$dirPath/main_production.dart',
          templates.startMainProductionModular(package, pathToAppModule),
        );
        createStaticFile(
          '$dirPath/main_qa.dart',
          templates.startMainQaModular(package, pathToAppModule),
        );
      }
      break;
  }

  if (useEnviroment) {
    changeGitIgnore(
        dirPath.replaceFirst('lib', ''),
        '''.idea/libraries
.idea/modules.xml
.idea/workspace.xml''',
        '.idea/');
  }
}
