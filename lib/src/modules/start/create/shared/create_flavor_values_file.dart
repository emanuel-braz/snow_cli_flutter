import 'package:flutter_snow_blower/src/templates/start/app_flavor_values.dart';
import 'package:flutter_snow_blower/src/utils/file_utils.dart';

void createAppFlavorValuesFile(String dirPath, bool haveInjectable) {
  createStaticFile(
    '$dirPath/app_flavor_values.dart',
    flavorValues(haveInjectable),
  );
}
