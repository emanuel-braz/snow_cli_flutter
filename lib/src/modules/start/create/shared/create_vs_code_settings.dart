part of './../create.dart';

void _createVsCodeSettings([String package = '', bool useFlavors = false]) {
  createStaticFile(
    package + '/.vscode/launch.json',
    useFlavors
        ? templates.launchSettingsVsCodeFlavor()
        : templates.launchSettingsVsCode(),
  );
}
