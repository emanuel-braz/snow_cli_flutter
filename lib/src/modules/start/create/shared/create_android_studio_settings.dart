part of './../create.dart';

void _createAndroidStudioSettings([
  String package = '',
  bool useFlavors = false,
]) {
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_Dev___DEBUG.xml',
    useFlavors
        ? templates.settingsDevDebugAndroidStudioFlavor()
        : templates.settingsDevDebugAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_Dev___PROFILE.xml',
    useFlavors
        ? templates.settingsDevProfileAndroidStudioFlavor()
        : templates.settingsDevProfileAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_Dev___RELEASE.xml',
    useFlavors
        ? templates.settingsDevReleaseAndroidStudioFlavor()
        : templates.settingsDevReleaseAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_Prod___DEBUG.xml',
    useFlavors
        ? templates.settingsProdDebugAndroidStudioFlavor()
        : templates.settingsProdDebugAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_Prod___PROFILE.xml',
    useFlavors
        ? templates.settingsProdProfileAndroidStudioFlavor()
        : templates.settingsProdProfileAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_Prod___RELEASE.xml',
    useFlavors
        ? templates.settingsProdReleaseAndroidStudioFlavor()
        : templates.settingsProdReleaseAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_QA___DEBUG.xml',
    useFlavors
        ? templates.settingsQaDebugAndroidStudioFlavor()
        : templates.settingsQaDebugAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_QA___PROFILE.xml',
    useFlavors
        ? templates.settingsQaProfileAndroidStudioFlavor()
        : templates.settingsQaProfileAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_QA___RELEASE.xml',
    useFlavors
        ? templates.settingsQaReleaseAndroidStudioFlavor()
        : templates.settingsQaReleaseAndroidStudio(),
  );
}
