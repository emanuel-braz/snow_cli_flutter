part of './create_mvc_modular.dart';

void _createAppWidgetAndModule(String package, bool complete) {
  createStaticFile(
    libPath('app_module.dart'),
    complete
        ? templates.startAppModuleModularComplete(package)
        : templates.startAppModuleModular(package),
  );
  createStaticFile(
    libPath('app_widget.dart'),
    complete
        ? templates.startAppWidgetComplete(package)
        : templates.startAppWidgetModular(package),
  );
}
