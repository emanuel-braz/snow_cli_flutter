import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';

String dioClientTest(String package, Architecture architecture) => '''
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:mobx/mobx.dart';
import 'package:$package/${architecture == Architecture.cleanArchitecture ? "data/remote" : "src/shared/clients"}/dio_client.dart';
import 'package:$package/app_flavor_values.dart';

void main() {
  DioClient dio;

  setUp(() {
    FlavorConfig(
      flavor: Flavor.production,
      values: AppFlavorValues(
        baseUrl: "", 
        features: null,
      ),
    );
    dio = DioClient();
  });

  test('Dio client test', () async {
    final resObservable = dio.get("wrongPath").asObservable();
    expect(resObservable, isNotNull);
    expect(resObservable.status, FutureStatus.pending);
    final res = await resObservable.catchError((e){});
    expect(resObservable.status, FutureStatus.rejected);
    expect(res, isNull);
  });
}
''';
