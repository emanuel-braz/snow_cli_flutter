String flavorValues(bool haveInjectable) => '''
import 'package:flutter/foundation.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
${haveInjectable ? "import 'package:injectable/injectable.dart';" : ""}

class AppFlavorValues implements FlavorValues{
  const AppFlavorValues({
    @required this.baseUrl, 
    this.anotherUrl,
    @required Map<String, bool> Function() features,
  }) : _features = features;

  final String baseUrl;
  final String anotherUrl;
  final Map<String, bool> Function() _features;
  Map<String, bool> get features => _features();
  //Add other flavor specific values, e.g database name
}

${haveInjectable ? "const qa = Environment('qa');" : ""}
''';
