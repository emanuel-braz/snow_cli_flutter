String splashControllerClean(String package) => '''
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:$package/di/di.dart';
import 'package:$package/domain/entities/auth_status.dart';
import 'package:$package/domain/usecases/auth/get_auth_status_stream_use_case.dart';
import 'package:$package/domain/usecases/auth/get_user_use_case.dart';

part 'splash_controller.g.dart';

class SplashController = _SplashControllerBase with _\$SplashController;

abstract class _SplashControllerBase with Store {

  final getUser = getIt<GetUserUseCase>();
  final authStatusStream = getIt<GetAuthStatusStreamUseCase>()();

  _SplashControllerBase() {
    authStatusStream.listen(_authListener);
    getUser();
  }

  void _authListener(AuthStatus status) {
    if (status == AuthStatus.login) {
      Modular.to.popUntil((route) => route.isFirst);
      Modular.to.pushReplacementNamed('/home');
    } else if (status == AuthStatus.logoff) {
      Modular.to.popUntil((route) => route.isFirst);
      Modular.to.pushReplacementNamed('/login');
    }
  }
}
''';
