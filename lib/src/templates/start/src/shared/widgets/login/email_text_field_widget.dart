import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';

String emailTextFieldWidget(
    String package, String srcOrApp, Architecture architecture) {
  String imports;

  switch (architecture) {
    case Architecture.cleanArchitecture:
      imports =
          '''import 'package:$package/$srcOrApp/utils/validators.dart';''';
      break;
    case Architecture.mvcModular:
      imports =
          '''import 'package:$package/$srcOrApp/shared/helpers/validators.dart';''';
      break;
  }

  return '''
import 'package:flutter/material.dart';
$imports

class EmailTextFieldWidget extends StatelessWidget {
  final TextEditingController controller;
  final void Function(String) onSaved;
  final String label;

  const EmailTextFieldWidget({
    Key key,
    this.controller,
    this.onSaved,
    this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      onSaved: onSaved,
      validator: Validators.email,
      decoration: InputDecoration(labelText: label),
    );
  }
}
''';
}
