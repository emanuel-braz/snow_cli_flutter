String authLocalDataSource() => '''
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';

@injectable
class AuthLocalDataSource {

  final FlutterSecureStorage _secureStorage;
  static const authInfoKeyDb = "auth_info";

  AuthLocalDataSource(this._secureStorage);

  Future<String> getAuthInfo() async {
    final json = await _secureStorage.read(key: authInfoKeyDb);
    if(json == null) return null;
    return json;
  }

  Future<void> saveAuthInfo(String authInfoJson) async {
    return _secureStorage.write(key: authInfoKeyDb, value: authInfoJson);
  }

  Future<void> deleteAuthInfo() {
    return _secureStorage.delete(key: authInfoKeyDb);
  }
}
''';
