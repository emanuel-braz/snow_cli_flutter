String someTable() => '''
import 'package:moor/moor.dart';

class SomeTable extends Table{

  TextColumn get uid => text()();
  TextColumn get someField => text()();
 
  @override
  Set<Column> get primaryKey => {uid};

}
''';
