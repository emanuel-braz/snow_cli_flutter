String indexMapper(bool complete) => '''
export 'user_mapper.dart';
${complete ? "export 'auth_mapper.dart';" : ""}
''';
