String localModuleClean(bool haveFlutterSecureStorage) => '''
${haveFlutterSecureStorage ? "import 'package:flutter_secure_storage/flutter_secure_storage.dart';" : ""}
import 'package:injectable/injectable.dart';

// Use this module to inject your third-party dependencies like [FlutterSecureStorage]
// E.g. @singleton FlutterSecureStorage get secureStorage => FlutterSecureStorage();

@module
abstract class LocalModule{

  ${haveFlutterSecureStorage ? "@singleton\nFlutterSecureStorage get secureStorage => FlutterSecureStorage();" : ""}

}
''';
