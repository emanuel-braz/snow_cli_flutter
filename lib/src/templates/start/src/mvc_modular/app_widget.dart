String startAppWidget(String package) => '''
import 'package:flutter/material.dart';
import 'package:$package/src/modules/home/home_module.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Snow Blower App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeModule(),
    );
  }
}
  ''';

String startAppWidgetModular(String package) => '''
import 'package:flutter/material.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:$package/src/styles/app_theme_data.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SnowMaterialApp(
      theme: AppThemeData.themeDataLight,
      darkTheme: AppThemeData.themeDataDark,
    );
  }
}
  ''';

String startAppWidgetComplete(String package) => '''
import 'package:flutter/material.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:$package/src/styles/app_theme_data.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SnowMaterialApp(
      theme: AppThemeData.themeDataLight,
      darkTheme: AppThemeData.themeDataDark,
    );
  }
}
  ''';
