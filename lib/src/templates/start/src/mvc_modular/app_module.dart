String startAppModuleModular(String pkg) => '''
import 'package:$pkg/src/shared/clients/dio_client.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:$pkg/src/shared/clients/hive_client.dart';

import 'app_widget.dart';
import 'modules/home/home_module.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => DioClient()),
        Bind((i) => HiveClient()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, module: HomeModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
  ''';

String startAppModuleModularComplete(String pkg) => '''
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:$pkg/src/modules/login/login_module.dart';
import 'package:$pkg/src/shared/auth/auth_store.dart';
import 'package:$pkg/src/shared/auth/repositories/auth_repository.dart';
import 'package:$pkg/src/shared/auth/repositories/auth_repository_interface.dart';
import 'package:$pkg/src/shared/clients/dio_client.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:$pkg/src/shared/clients/hive_client.dart';

import 'app_widget.dart';
import 'modules/home/home_module.dart';
import 'modules/splash/splash_page.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => DioClient()),
        Bind<AuthRepository>((i) => AuthRepositoryDefault()),
        Bind((i) => HiveClient()),
        Bind((i) => AuthStore(), singleton: true),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          Modular.initialRoute,
          child: (context, args) => SplashPage(),
          transition: TransitionType.noTransition,
        ),
        ModularRouter('/login',
            module: LoginModule()),
        ModularRouter('/home',
            module: HomeModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
  ''';
