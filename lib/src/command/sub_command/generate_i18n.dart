import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class GenerateI18nSubCommand extends CommandBase {
  @override
  final name = 'i18n';
  @override
  final description = 'Creates a i18n file';

  GenerateI18nSubCommand() {
    argParser.addFlag('notest',
        abbr: 'n', negatable: false, help: 'no create file test');
  }

  @override
  Future<void> run() async {
    if (argResults.rest.isEmpty) {
      throw UsageException('value not passed for a i18n command', usage);
    } else {
      await Generate.i18n(argResults.rest.first, !argResults['notest']);
    }
    super.run();
  }
}

class GenerateI18nAbbrSubCommand extends GenerateI18nSubCommand {
  @override
  final name = 'i';
}
