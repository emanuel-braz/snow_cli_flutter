import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class GenerateEntitySubCommand extends CommandBase {
  @override
  final name = 'entity';
  @override
  final description = 'Creates a entity (Only for Clean Architecture)';

  GenerateEntitySubCommand();

  @override
  Future<void> run() async {
    if (argResults.rest.isEmpty) {
      throw UsageException('value not passed for Entity command', usage);
    } else {
      await Generate.entity(argResults.rest.first);
    }
    super.run();
  }
}

class GenerateEntityAbbrSubCommand extends GenerateEntitySubCommand {
  @override
  final name = 'e';
}
