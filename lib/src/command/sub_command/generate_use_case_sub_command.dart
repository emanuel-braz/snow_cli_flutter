import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class GenerateUseCaseSubCommand extends CommandBase {
  @override
  final name = 'use_case';
  @override
  final description = 'Creates a Use case (Only for Clean Architecture)';

  GenerateUseCaseSubCommand();

  @override
  Future<void> run() async {
    if (argResults.rest.isEmpty) {
      throw UsageException('value not passed for Use case command', usage);
    } else {
      await Generate.useCase(argResults.rest.first);
    }
    super.run();
  }
}

class GenerateUseCaseAbbrSubCommand extends GenerateUseCaseSubCommand {
  @override
  final name = 'u';
}
