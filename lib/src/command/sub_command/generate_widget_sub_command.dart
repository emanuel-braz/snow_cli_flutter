import 'dart:async';

import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class GenerateWidgetSubCommand extends CommandBase {
  @override
  final name = 'widget';
  @override
  final description = 'Creates a widget';

  GenerateWidgetSubCommand() {
    argParser.addFlag('controller',
        abbr: 'c',
        negatable: false,
        help: 'Creates a page with controller file');
  }

  @override
  Future<FutureOr<void>> run() async {
    if (argResults.rest.isEmpty) {
      throw UsageException('value not passed for a module command', usage);
    } else {
      await Generate.widget(argResults.rest.first, argResults['controller']);
    }
    super.run();
  }
}

class GenerateWidgetAbbrSubCommand extends GenerateWidgetSubCommand {
  @override
  final name = 'w';
}
