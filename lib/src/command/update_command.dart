import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class UpdateCommand extends CommandBase {
  @override
  final name = 'update';
  @override
  final description = 'Update a new package or packages.';

  UpdateCommand() {
    argParser.addFlag('dev',
        negatable: false, help: 'Update a package in a dev dependency');
  }

  @override
  Future<void> run() {
    if (argResults.rest.isEmpty) {
      throw UsageException('value not passed for a module command', usage);
    } else {
      return update(argResults.rest, argResults['dev']);
    }
  }
}
