import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';

class ObjectGenerate {
  final String name;
  final String packageName;
  final String import;
  final String type;
  final String module;
  final String pathModule;
  final Architecture architecture;
  final dynamic addicionalInfo;

  ObjectGenerate({
    this.addicionalInfo,
    this.architecture,
    this.type,
    this.name,
    this.packageName,
    this.import,
    this.module,
    this.pathModule,
  });
}
