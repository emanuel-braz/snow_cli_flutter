<!--
# Leia me primeiro!

Defina o titulo para: `Release de Segurança: 1.3.X, 1.4.X e 0.9.X`
-->

## Tarefas para as Releases

<!-- Esse processo ainda iremos definir se tem duvidas, entre contato pelo canal do slack #dev 
    https://snowmanlabs.slack.com/messages/C0299TJ97 tire suas duvidas antes de prosseguir 
    e leia os topicos abaixo para te ajudar. -->

- https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/release-manager.md
- https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/developer.md
- https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/security-engineer.md

## Version issues:

* 1.4.X: {link da tarefa de release}
* 1.3.X: {link da tarefa de release}
* 0.9.X: {link da tarefa de release}

## Issues de Segurança:

* {https://gitlab.com/snoman-labs/PROJETO_NOME/issues link}

| Version | MR |
|---------|----|
| 1.4 | {https://gitlab.org/snoman-labs/PROJETO_NOME/merge_requests/ link} |
| 1.3 | {https://gitlab.org/snoman-labs/PROJETO_NOME/erge_requests/ link} |
| 0.9 | {https://gitlab.org/snoman-labs/PROJETO_NOME/merge_requests/ link} |
| master | {https://gitlab.org/snoman-labs/PROJETO_NOME/merge_requests/ link} |

## QA
{link de issue de QA}

## Blog post

<!-- se precisar informar sobre essa atualizacao em algum ligar informar aqui. -->

## Email de notificação
{https://gitlab.com/snoman-labs/PROJETO_NOME/issues/ link}

/label ~security
/confidential
