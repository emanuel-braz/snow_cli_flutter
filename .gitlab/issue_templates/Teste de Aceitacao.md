## Detalhes
- **Nome de alternancia da Feature**: `FEATURE_NAME`
- **Versão Necessária**: `vX.X`

--------------------------------------------------------------------------------

## 1. Preparação

- [ ] **Controllers e workers**:
  1. Por favor, link para o dashboard dos workers, e os controllers e ações que podem ser impactadas
  2. ...
  3. ...

## 2. Avaliação de Desenvolvimento

#### Verificar versões do servidor de Dev

- [ ] `PROJETO_NAME`: `http://url-servidor-dev.projeto_name.com`

#### Habilitar no ambiente de Dev:
<!-- Defina aqui as variaveis de ambiente, comandos e links para ativar ou deixar o ambiente de dev funcionado para o teste -->
- [ ] `bin/command_y` - comando pra fazer y de exemplo
- [ ] Mais infos em [`#dev`](https://snowmanlabs.slack.com/messages/C0299TJ97)

Depois deixe executando enquanto monitora e performa alguns testes através da web, api ou SSH.

#### Monitor

- [ ] [Monitor de Serviço Usado - https://monitoramento.projeto-name.com](https://monitoramento.projeto-name.com)
- [ ] [Monitor de Logs Externos - https://log.projeto-name.com](https://log.projeto-name.com)
- [ ] [Monitor de Erros Externo - https://sentry.projeto-name.com/projeto/?query=is%3Aunresolved](https://sentry.projeto-name.com/projeto/?query=is%3Aunresolved)

## 2. Avaliação de Staging

#### Verificar versões do servidor de Staging

- [ ] `PROJETO_NAME`: `http://url-servidor-staging.projeto_name.com`

#### Habilitar no ambiente de Staging
<!-- Defina aqui as variaveis de ambiente, comandos e links para ativar ou deixar o ambiente de dev funcionado para o teste -->
- [ ] `bin/command_y` - comando pra fazer y de exemplo
- [ ] Mais infos em [`#dev`](https://snowmanlabs.slack.com/messages/C0299TJ97)

Depois deixe executando enquanto monitora por pelo menos **15 minutos** enquanto performa alguns testes através da web, api ou SSH.

#### Monitor

- [ ] [Monitor de Serviço Usado - https://monitoramento.projeto-name.com](https://monitoramento.projeto-name.com)
- [ ] [Monitor de Logs Externos - https://log.projeto-name.com](https://log.projeto-name.com)
- [ ] [Monitor de Erros Externo - https://sentry.projeto-name.com/projeto/?query=is%3Aunresolved](https://sentry.projeto-name.com/projeto/?query=is%3Aunresolved)

## 4. Verificar versão do servidor de Produção/Production

- [ ] `PROJETO_NAME`: `http://url-servidor-prod.projeto_name.com`

## 5. Verificar impacto inicial

- [ ] Habilitar para um grupo limitado de usuários, quando for por porcentagem deve se limitar a 1%.

Depois deixe executando enquanto monitora por pelo menos **15 minutos** enquanto performa alguns testes através da web, api ou SSH.

#### Monitor

- [ ] [Monitor de Serviço Usado - https://monitoramento.projeto-name.com](https://monitoramento.projeto-name.com)
- [ ] [Monitor de Logs Externos - https://log.projeto-name.com](https://log.projeto-name.com)
- [ ] [Monitor de Erros Externo - https://sentry.projeto-name.com/projeto/?query=is%3Aunresolved](https://sentry.projeto-name.com/projeto/?query=is%3Aunresolved)

## 6. Verificação de baixo impacto

- [ ] Habilitar para um grupo de usuários maior, quando for por porcentagem deve se limitar a 10%.

Depois deixe executando enquanto monitora por pelo menos **30 minutos** enquanto performa alguns testes através da web, api ou SSH.

#### Monitor

- [ ] [Monitor de Serviço Usado - https://monitoramento.projeto-name.com](https://monitoramento.projeto-name.com)
- [ ] [Monitor de Logs Externos - https://log.projeto-name.com](https://log.projeto-name.com)
- [ ] [Monitor de Erros Externo - https://sentry.projeto-name.com/projeto/?query=is%3Aunresolved](https://sentry.projeto-name.com/projeto/?query=is%3Aunresolved)

## 7. Avaliação de Médio impacto

- [ ] Habilitar para um grande grupo de usuários, quando for por porcentagem deve se limitar a 50%

Depois deixe executando enquanto monitora por pelo menos **12 horas** enquanto performa alguns testes através da web, api ou SSH.

#### Monitor

- [ ] [Monitor de Serviço Usado - https://monitoramento.projeto-name.com](https://monitoramento.projeto-name.com)
- [ ] [Monitor de Logs Externos - https://log.projeto-name.com](https://log.projeto-name.com)
- [ ] [Monitor de Erros Externo - https://sentry.projeto-name.com/projeto/?query=is%3Aunresolved](https://sentry.projeto-name.com/projeto/?query=is%3Aunresolved)

## 8. Avaliação de Impacto Completo

- [ ] Habilitar para todos os usuários

Então deixe executando enquanto monitora por pelo menos **1 semana**.

#### Monitor

- [ ] [Monitor de Serviço Usado - https://monitoramento.projeto-name.com](https://monitoramento.projeto-name.com)
- [ ] [Monitor de Logs Externos - https://log.projeto-name.com](https://log.projeto-name.com)
- [ ] [Monitor de Erros Externo - https://sentry.projeto-name.com/projeto/?query=is%3Aunresolved](https://sentry.projeto-name.com/projeto/?query=is%3Aunresolved)

#### Sucesso?

- [ ] Remove a trava de limite de usuários do código, e feche a issue junto com o MR.
