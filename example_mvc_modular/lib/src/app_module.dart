import 'package:example_mvc_modular/src/shared/clients/hive_client.dart';
import 'package:example_mvc_modular/src/todo_repository.dart';
import 'package:example_mvc_modular/src/shared/clients/dio_client.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'app_widget.dart';
import 'modules/home/home_module.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => TodoRepository()),
        Bind((i) => DioClient()),
        Bind((i) => HiveClient()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, module: HomeModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
