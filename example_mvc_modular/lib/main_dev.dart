import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';
import 'package:example_mvc_modular/src/app_module.dart';
import 'package:example_mvc_modular/src/shared/constants/constants.dart';
import 'package:example_mvc_modular/src/shared/helpers/error_mapper.dart';

void main() {
  RunAppSnow(
    ModularApp(
      module: AppModule(),
    ),
    flavorValues: Constants.flavorDev,
    getItInit: () => Resource.setErrorMapper(ErrorMapper.from),
    flavor: Flavor.dev,
    enableDevicePreview: false,
  );
}
